package com.example.robin.androidproject;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import static android.app.PendingIntent.getActivity;

public class MainActivity extends Activity {

    ArrayList<String> listItems =new ArrayList<String> ();

    ArrayAdapter<String> adapter;

    ListView listView_player ;

    ArrayList<Player>  Players = new ArrayList<Player>();

    Button button_start ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setBackgroundColor(Color.argb(255,185,255,130));
        setContentView(R.layout.activity_main);

        listView_player = (ListView) findViewById(R.id.listView_Players);

        button_start = (Button) findViewById(R.id.button_start);

        adapter = new ArrayAdapter<String>(this,R.layout.item_list,R.id.textView_model,listItems);

        if(listView_player == null)
        {
            Log.d("listView","Is Null");
        }

        listView_player.setAdapter(adapter);

        Log.d("adapter","create");




        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (listItems.size() > 0)
                {
                    Intent intent = new Intent(getApplicationContext(), PledgeActivity.class);

                    intent.putExtra("Players", listItems);

                    startActivity(intent);

                } else
                    {
                    Toast.makeText(getApplicationContext(), "Il faut au moins 1 joueur WSH !", Toast.LENGTH_LONG).show();
                }

            }
        }
        );
    }


    public void popupEditText(View view) {

     //   Log.d("clickbutton","boom!!!");
        final EditText input = new EditText(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        input.setInputType(InputType.TYPE_CLASS_TEXT );
        builder.setView(input);


        builder.setMessage("Nouveau joueur")
                .setTitle("Quel est son prénom? (ou surnom c'est plus marrant) ");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User clicked OK button

                listItems.add(input.getText().toString());
             /*   Player player_tmp = new Player(input.getText().toString());
                player_tmp.setId(Players.size());
                Players.add(player_tmp);*/

                Log.d("adapter","ADD");
            }
        });

        builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }



}
