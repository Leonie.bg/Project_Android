package com.example.robin.androidproject;

/**
 * Created by Robin on 19/01/2018.
 */

public class Player
{
    public String Name ;
    public int Id;
    public int Gulp;


    public Player (String _name)
    {
        Name = _name;
    }

    public void AddGulp(int input)
    {
        Gulp += input;
    }

    public int getGulp() {
        return Gulp;
    }

    public String getName() {
        return Name;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getId() {
        return Id;
    }

    public void setGulp(int gulp) {
        Gulp = gulp;
    }

    public void setName(String name) {
        Name = name;
    }
}
