package com.example.robin.androidproject;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;

public class PledgeActivity extends Activity {

    ArrayList<Pledge> pledges = new ArrayList<Pledge>();
    ArrayList<Player> players  = new  ArrayList<Player>();
    ArrayList<String> textPledge =  new ArrayList<String>();
    ArrayList<String> namePlayer =  new ArrayList<String>();
    int interrator = 0;
    TextView tv_pledge ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setBackgroundColor(Color.argb(255,185,255,130));

        setContentView(R.layout.activity_pledge);

        namePlayer =  getIntent().getExtras().getStringArrayList("Players");
        tv_pledge=  (TextView) findViewById(R.id.textView_pledge);
        setPlayer();

        initText();
        setAllPledges();
        displayPledge();






    }

    private void setPlayer()
    {
        for (String tmp : namePlayer)
        {
            Player player_tmp  =new Player(tmp);
            player_tmp.setId(players.size());

            players.add(player_tmp);

        }
    }

    private void setAllPledges()
    {

        for(int i =0 ; i< 10 ; i++)
        {
            Random rand = new Random();

            Player player_tmp =  players.get(rand.nextInt(players.size()-1));
            Pledge pledge = new Pledge(player_tmp.getName()+textPledge.get(rand.nextInt(textPledge.size()-1)),"","Canard");
            //pledge.setPlayer(pledge);

            pledges.add(pledge);
        }

    }

    private  void initText()
    {
        textPledge.add(" bois 3 gorgées, parce que c'est comme ça!");
        textPledge.add(" raconte une blague, ceux qui on rigolé boivent 2 gorgées !");
        textPledge.add(" a tué tout les fans de Jul, on peut enfin parler en Francais soutenu.");
    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    private void displayPledge()
    {

        tv_pledge.setText(pledges.get(interrator).getText());

    }

    private  void pledgeNext (View view)
    {
        if(interrator+1 <pledges.size())
        {
            interrator ++;
        }
        else
        {
            tv_pledge.setText("C'est bon vous êtes bouré ( ou pas)  MDR !");
        }

        displayPledge();
    }
}
