package com.example.robin.androidproject;

/**
 * Created by Robin on 20/01/2018.
 */

public class Pledge {

    String text;
    Player player;
    String auteur;
    String title;

    public Pledge ()
    {

    }

    public Pledge (String _text,String _title, String _auteur)
    {
        text = _text;
        auteur= _auteur;
        title =_title;

    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }
}
